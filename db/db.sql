
CREATE DATABASE IF NOT EXISTS sajunio2024;
USE sajunio2024;

CREATE TABLE IF NOT EXISTS Usuarios (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50) UNIQUE NOT NULL,
    password VARCHAR(100) NOT NULL,
    nombre VARCHAR(100),
    apellido VARCHAR(100),
    email VARCHAR(100)
);


CREATE TABLE IF NOT EXISTS DatosAire (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    latitud DECIMAL(10, 8) NOT NULL,
    longitud DECIMAL(11, 8) NOT NULL,
    aqi INT NOT NULL,
    FOREIGN KEY (username) REFERENCES Usuarios(username)
);

INSERT INTO Usuarios (username, password, nombre, apellido, email) VALUES
('alvaro', '12345', 'Alvaro', 'Socop', 'socop2412@gmail.com'),
('juan', '12345', 'Juan', 'Perez', 'juan@example.com');

INSERT INTO DatosAire (username, latitud, longitud, aqi) VALUES
('alvaro', 14.58759144, -90.55307114, 75),
('juan', 14.64733104, -90.55397843, 60);
