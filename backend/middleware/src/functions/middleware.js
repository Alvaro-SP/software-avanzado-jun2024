const db = require('../db/conexion');
// function crearLog(middleware, entrada, salida, esError) {
//     const log = {
//         Middleware: middleware,
//         Entrada: entrada,
//         Salida: salida,
//         EsError: esError
//     };
//     console.log("log: ", log);
//     const sql1 = `CALL crearLog(?,?,?,?)`;
//     const result = db.query(sql1, [log.Middleware, JSON.stringify(log.Entrada), JSON.stringify(log.Salida), log.EsError]);
//     console.log("log: ", log);
//     console.log("result: ", result);
// }

async function middleware(req, res, next) {
    if (req.query.api == "aire") {
        if (req.query.id == "guardar") {
            try {
                const fetch = await import('node-fetch');
                const response = await fetch.default(`${process.env.SERVICE_AIRE}/verperfil`, {
                    method: 'POST',
                    body: JSON.stringify(req.body),
                    headers: { 'Content-Type': 'application/json' }
                });

                if (!response.ok) {
                    throw new Error('Error de conexión con el servidor');
                }

                const json = await response.json();
                console.log(json);
                res.json(json);
            } catch (error) {
                console.error(error);
                res.status(500).json({ error: 'Error de conexión con el servidor' });
            }
        } else if (req.query.id == "otrafuncion1") {
            try {
                console.log("otrafuncion1");
            } catch (error) {
                console.error(error);
                res.status(500).json({ error: 'Error de conexión con el servidor' });
            }
        } 

    } else if (req.query.api == "usuario") {
        //aquí va el código para la api de usuario
        if (req.query.id == "profile") {
            try {
                const fetch = await import('node-fetch');
                const response = await fetch.default(`${process.env.SERVICE_USUARIO}/verperfil`, {
                    method: 'POST',
                    body: JSON.stringify(req.body),
                    headers: { 'Content-Type': 'application/json' }
                });

                if (!response.ok) {
                    throw new Error('Error de conexión con el servidor');
                }

                const json = await response.json();
                console.log(json);
                res.json(json);
            } catch (error) {
                console.error(error);
                res.status(500).json({ error: 'Error de conexión con el servidor' });
            }
        }

    } else if (req.query.api == "loginflow") {
        if (req.query.id == "registro") {
            try {
                const fetch = await import('node-fetch');
                const response = await fetch.default(`${process.env.SERVICE_LOGINFLOW}/registro`, {
                    method: 'POST',
                    body: JSON.stringify(req.body),
                    headers: { 'Content-Type': 'application/json' }
                });

                if (!response.ok) {
                    throw new Error('Error de conexión con el servidor');
                }

                const json = await response.json();
                console.log(json);
                // crearLog("LoginFlow-registro", req.body, json, json.Res)
                res.json(json);
            } catch (error) {
                console.error(error);
                // crearLog('LoginFlow-registro', req.body, { salida_error: 'Error de conexión con el servidor' }, true);
                res.status(500).json({ error: 'Error de conexión con el servidor' });
            }
        } else if (req.query.id == "login") {
            try {
                const fetch = await import('node-fetch');
                const response = await fetch.default(`${process.env.SERVICE_LOGINFLOW}/login`, {
                    method: 'POST',
                    body: JSON.stringify(req.body),
                    headers: { 'Content-Type': 'application/json' }
                });
                if (!response.ok) {
                    throw new Error('Error de conexión con el servidor');
                }

                const json = await response.json();
                console.log(json);
                // crearLog('LoginFlow-login', req.body, json, json.Res);
                res.json(json);
            } catch (error) {
                console.error(error);
                // crearLog('LoginFlow-login', req.body, { salida_error: 'Error de conexión con el servidor' }, true);
                res.status(500).json({ error: 'Error de conexión con el servidor' });
            }
        }
    }
}

module.exports = middleware;
