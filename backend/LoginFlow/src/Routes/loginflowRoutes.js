const express = require('express')
const router = express.Router()
const control = require('../Controllers/loginflowController')

router.get("/", control.index)
router.post("/registro", control.registro)
router.post("/login", control.login)

module.exports = router 