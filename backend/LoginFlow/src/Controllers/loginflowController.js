const mysql = require('mysql2');
const jwt = require('jsonwebtoken');
var dotenv = require("dotenv");
dotenv.config();

const config = {
    host: process.env.MYSQL_HOSTNAME,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    port: process.env.MYSQL_PORT,
};

exports.index = async (req, res) => {
    res.send({ "Saludos": "Funciona la api" });
};

exports.registro = async (req, res) => {
    try {
        const { nombre, usuario, correo, fecha, telefono, password } = req.body;

        let c = mysql.createConnection(config);
        c.connect(function (err) {
            if (err) {
                console.log(err);
                c.end();
                return res.status(400).jsonp({ Res: false, Mensaje: "Error en la conexion a la db" });
            }
            c.query(`SELECT * FROM Usuarios WHERE username='${usuario}' OR email='${correo}';`, async function (err, result, field) {
                if (err) {
                    console.log(err);
                    c.end();
                    return res.status(400).jsonp({ Res: false, Mensaje: "Error en la conexion a la db 2" });
                }
                if (result.length >= 1) {
                    if (result[0].username == usuario) {
                        c.end();
                        return res.jsonp({ Res: false, Mensaje: "Usuario (Nickname) ya existente" });
                    } else {
                        c.end();
                        return res.jsonp({ Res: false, Mensaje: "Correo ya existente" });
                    }
                }

                c.query(`INSERT INTO Usuarios (nombre, username, email, nacimiento, password, telefono) VALUES ('${nombre}', '${usuario}', '${correo}', '${fecha}', '${password}', '+502${telefono}');`,
                    function (err, result, field) {
                        if (err) {
                            console.log(err);
                            c.end();
                            return res.status(400).jsonp({ Res: false, Mensaje: "Error en la conexion a la db 3" });
                        }
                        c.end();
                        return res.jsonp({ Res: true, Mensaje: "Ok" });
                    });
            });
        });
    } catch (e) {
        console.log(e);
        res.status(400).jsonp({ Res: false });
    }
}

exports.login = async (req, res) => {
    try {
        const { usuario, password } = req.body;
        let c = mysql.createConnection(config);
        c.connect(function (err) {
            if (err) {
                console.log(err);
                c.end();
                return res.status(400).jsonp({ Res: false, Mensaje: "Error en la conexion a la db" });
            }

            c.query(`SELECT * FROM Usuarios WHERE username='${usuario}';`, async function (err, result, fields) {
                if (err) {
                    console.log(err);
                    c.end();
                    return res.status(400).jsonp({ Mensaje: "Error en la conexion a la db 2" });
                }

                if (result.length == 0) {
                    console.log(err);
                    c.end();
                    return res.jsonp({ Res: false, Mensaje: "Usuario no existente" });
                }

                const usuarioDb = result[0];

                if (usuarioDb.password != password) {
                    c.end();
                    return res.jsonp({ Res: false, Mensaje: "Password incorrecta" });
                }

                const token = jwt.sign({ id: usuarioDb.id, tipo: usuarioDb.tipo_usuario }, process.env.JWT_SECRET, { expiresIn: '1h' });
                c.end();
                return res.jsonp({ Res: true, Mensaje: 'Ok', Token: token });
            });
        });
    } catch (err) {
        console.log(err);
        res.status(400).jsonp({ Res: false });
    }
}
