import React, { useState, useEffect } from 'react';
import {MapContainer, TileLayer, Marker, Popup} from 'react-leaflet';
import './AirqualityMap.css'
import L from 'leaflet';
/***
 * 
 * @function AirqualityMap
 * @description This component is responsible for rendering the map and the markers on the map.
 * @returns AirqualityMap
 * 
 */
export default function AirqualityMap() {
    const [datafeed, setDatafeed] = useState({});

    useEffect(() => {
        fetch('https://api.waqi.info/feed/geo:14.587591445157098;-90.553071148824/?token=b272215050a4f21e5eb8f5832264c2cca4565a4b')
        .then(response => response.json())
        .then(data => {
            setDatafeed(data)
            console.log(datafeed)
            }
        )
    }, [])
    var myIcon = L.icon({
        iconUrl: 'https://static-00.iconduck.com/assets.00/position-icon-1435x2048-e1qga9xm.png',
        iconSize: [20, 50],
        iconAnchor: [22, 94],
        popupAnchor: [-3, -76],
        // shadowUrl: 'my-icon-shadow.png',
        // shadowSize: [68, 95],
        // shadowAnchor: [22, 94]
    });
    
    return (
        <div className="map-wrapper">
            <MapContainer center={[14.647331044236717, -90.55397843806679]} zoom={13} scrollWheelZoom={false}>
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {/* bucle para crear varios marker y popup en diferentes posiciones */}
                
                {datafeed.data && datafeed.data.aqi !== undefined && (
                    <Marker position={[14.647331044236717, -90.55397843806679]} icon={myIcon}>
                        <Popup>
                            <img
                                src="https://cdn-icons-png.flaticon.com/512/3095/3095177.png"
                                alt="AirVisual"
                                style={{ width: "65px", height:"65px" }}
                            />
                            <br></br>
                            {datafeed.data.aqi.toString()}
                        </Popup>
                    </Marker>
                )}
            </MapContainer>
        </div>
    );
}