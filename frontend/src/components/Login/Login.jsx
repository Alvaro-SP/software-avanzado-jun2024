import React, { useState } from 'react';
import axios from 'axios';

const Login = ({ onLogin}) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = async (e) => {
        e.preventDefault();

        try {
            const response = await axios.post(`${import.meta.env.VITE_API_URL}api=loginflow&id=login`, {
                usuario: username,
                password: password
            });
            console.log(response.data)
            if (response.data.Res) {
                // Llamar a la función de inicio de sesión proporcionada por el padre
                onLogin(response.data.Token);
                setIsAuthenticated(false)
            } else {
                // Manejar error de inicio de sesión
                console.error('Error de inicio de sesión:', response.data.Mensaje);
            }
        } catch (error) {
            // Manejar errores de red u otros errores
            console.error('Ocurrió un error:', error);
        }
    };

    return (
        <div style={{ backgroundImage: 'url("https://cdn.thenewstack.io/media/2016/02/Docker.png")', backgroundSize: 'cover', height: '90vh', width: '90vw', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <div style={{ backgroundColor: 'rgba(0, 0, 0, 0.6)', padding: '20px', borderRadius: '10px', maxWidth: '400px', width: '80%' }}>
                <h2>Iniciar sesión</h2>
                <form onSubmit={handleLogin}>
                    <div>
                        <label htmlFor="username">Username:</label>
                        <input
                            type="text"
                            id="username"
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                        />
                    </div>
                    <br />
                    <div>
                        <label htmlFor="password">Password:</label>
                        <input
                            type="password"
                            id="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </div>
                    <br />
                    <br />
                    <button type="submit">Iniciar sesión</button>
                </form>
            </div>
        </div>
    );
};

export default Login;
