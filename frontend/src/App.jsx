import { useState, useEffect } from 'react';
import AirqualityMap from './components/AirqualityMap';
import './App.css';
import Login  from './components/Login/Login';
import { Button, Row, Col, Container } from 'react-bootstrap';
import 'leaflet/dist/leaflet.css';
// import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    const [isAuthenticated, setIsAuthenticated] = useState(false);

    useEffect(() => {
        const token = sessionStorage.getItem("token");
        if (token) {
            setIsAuthenticated(true);
        }
    }, []);

    const handleLogin = (token) => {
        sessionStorage.setItem("token", token);
        setIsAuthenticated(true);
    };

    return (
        <>
            {!isAuthenticated ? (
                <Login  onLogin={handleLogin}/>
            ) : (
                <Container>
                    <Row>
                        <Col>
                            <h1>Mapa de la calidad del aire</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <AirqualityMap />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button variant="primary" onClick={() => setIsAuthenticated(false)}>Cerrar sesión</Button>
                        </Col>
                    </Row>
                </Container>
            )}
        </>
    );
}

export default App;
